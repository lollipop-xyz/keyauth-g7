package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/go-course/keyauth-g7/apps/policy/api"
	_ "gitee.com/go-course/keyauth-g7/apps/role/api"
	_ "gitee.com/go-course/keyauth-g7/apps/token/api"
	_ "gitee.com/go-course/keyauth-g7/apps/user/api"
)
