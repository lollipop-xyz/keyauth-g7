package impl

import (
	"context"

	"gitee.com/go-course/keyauth-g7/apps/endpoint"
)

func (s *service) RegistryEndpoint(ctx context.Context, req *endpoint.EndpiontSet) (
	*endpoint.RegistryResponse, error) {
	if err := s.save(ctx, req); err != nil {
		return nil, err
	}
	resp := endpoint.NewRegistryResponse()
	return resp, nil
}
